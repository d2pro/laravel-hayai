const mix = require('laravel-mix')
const webpack = require('webpack')

mix.options({
    terser: {
        terserOptions: {
            compress: {
                drop_console: true
            }
        }
    }
})
    .setPublicPath("public")
    .js("resources/js/index.js", "public")
    .version()
    .copy('resources/img', 'public/img')
    // .copy('public', '../../horizontest/public/vendor/horizon')
    .webpackConfig({
        resolve: {
            symlinks: false,
            alias: {
                'vue$': 'vue/dist/vue.runtime.esm.js',
                "@": path.resolve(__dirname, "resources/js/")
            }
        },
        plugins: [new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)],
    });
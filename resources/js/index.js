import * as setup from './setup'
import * as components from './components'
import * as mixins from './mixins'

import { merge, renameProperty, queryStringToObject } from './utils/helpers'
import config, { setOptions, setVueInstance } from './utils/config'
import { use, registerComponent, registerComponentProgrammatic, registerCollection } from './utils/plugins'

import ConfigComponent from './utils/ConfigComponent'
import HasTooltipMixin from './components/tooltip/HasTooltip'
import FormFieldMixin from './mixins/FormFieldMixin'

import VueMeta from 'vue-meta'
import VTooltip from 'v-tooltip'

const Hayai = {
    install(Vue, options = {}) {
        // Vue instance
        setVueInstance(Vue)

        // Options
        setOptions(merge(config, options, true))

        // Components
        for (let componentKey in components) {
            Vue.use(components[componentKey])
        }

        // Config component
        registerComponentProgrammatic(Vue, 'config', ConfigComponent)

        // Mixins
        _.forEach(mixins, (mixin, key) => {
            registerCollection(Vue, 'mixins', key, mixin)
        })
        registerCollection(Vue, 'mixins', HasTooltipMixin.name, HasTooltipMixin)
        registerCollection(Vue, 'mixins', FormFieldMixin.name, FormFieldMixin)
        // Rutas con Ziggy
        Vue.mixin({ methods: { route: window.route } })

        // Plugins
        // Inertia
        Vue.use(setup.Inertia.InertiaApp)
        registerCollection(Vue, 'functions', 'renderInertia', setup.Inertia.render)
        registerCollection(Vue, 'functions', 'registerLayout', setup.Inertia.registerLayout)
        registerCollection(Vue, 'functions', 'registerComponent', function(component) {
            let registered = registerComponent(Vue, component)
            registerCollection(Vue, 'components', component.name, registered)
        })
        // Axios
        setup.AxiosSetup(Vue)
        // Varios
        Vue.use(VueMeta)
        Vue.use(VTooltip, {
            defaultOffset: 1,
        })

        // Global Functions
        registerCollection(Vue, 'functions', 'renameProperty', renameProperty)
        registerCollection(Vue, 'functions', 'queryStringToObject', queryStringToObject)

        window.Hayai = Vue.prototype.$hayai
    },
}

use(Hayai)

export default Hayai

// export all components as vue plugin
export * from './components'
// export programmatic component
export { ModalProgrammatic } from './components/modal'
// export { LoadingProgrammatic } from './components/loading'
// export { NotificationProgrammatic } from './components/notification'
// export { SnackbarProgrammatic } from './components/snackbar'
// export { ToastProgrammatic } from './components/toast'
export { default as ConfigProgrammatic } from './utils/ConfigComponent'
// export helpers
export * from './utils/helpers'

import { VueInstance } from './config'

export const vueInstance = (localVue) => {
    return (typeof window !== 'undefined') && window.Vue ? window.Vue : (localVue || VueInstance)
}

export const use = (plugin) => {
    if (typeof window !== 'undefined' && window.Vue) {
        window.Vue.use(plugin)
    }
}

export const registerComponent = (Vue, component) => {
    return Vue.component(component.name, component)
}

export const registerComponentProgrammatic = (Vue, property, component) => {
    if (! Vue.prototype.$hayai) {
        Vue.prototype.$hayai = {}
    }

    Vue.prototype.$hayai[property] = component
}

export const registerCollection = (Vue, collectionName, key, element) => {
    if (! Vue.prototype.$hayai) {
        Vue.prototype.$hayai = {}
    }
    if (! Vue.prototype.$hayai[collectionName]) {
        Vue.prototype.$hayai[collectionName] = {}
    }

    Vue.prototype.$hayai[collectionName][key] = element
}

/**
 * Mixin para páginas con formulario para edición de un modelo
 */

export default {
  props: {
    instance: Object,
  },

  data() {
    return {
      form: {},
    }
  },

  provide() {
    return {
      form: this.__getForm
    }
  },

  mounted() {
    this.__createFormObject()
  },

  methods: {
    __createFormObject() {
      if (this.instance) {
        this.form = Vue.util.extend({}, this.instance)
      }
    },
    __getForm() {
      return this.form
    }
  },
}

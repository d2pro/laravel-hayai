/**
 * Adds a keypress listener to the component
 */

export default {
    created() {
        window.addEventListener('keypress', this.handleKeypress)
        window.addEventListener('keydown', this.handleKeydown)
    },

    destroyed() {
        window.removeEventListener('keypress', this.handleKeypress)
        window.removeEventListener('keydown', this.handleKeydown)
    },

    methods: {
        // You must override these methods in the component
        handleKeypress(e) {
            // let cmd = String.fromCharCode(e.keyCode).toLowerCase()
            // // do stuff
            // console.log('[ComponentKeypressMixin:keypress]: keyCode=', e.keyCode, cmd)
        },
        handleKeydown(e) {
            // let cmd = String.fromCharCode(e.keyCode).toLowerCase()
            // // do stuff
            // console.log('[ComponentKeypressMixin:keydown]: keyCode=', e.keyCode, cmd)
        }
    }

}
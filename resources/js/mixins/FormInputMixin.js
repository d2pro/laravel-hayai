/**
 * Mixin para componentes tipo input
 */

export default {
  inheritAttrs: false,

  inject: {
    form: { default: {} }
  },

  props: {
    id: {
      type: String,
      default() {
        return this.$options._componentTag + '-' + this._uid
      }
    },
    field: String,
    value: [String, Number, Boolean, Array, Object],
  },

  data() {
    return {
      internalValue: null,
    }
  },

  computed: {
    listeners() {
      const { input, ...listeners } = this.$listeners;
      return listeners;
    },
    __form() {
      if (typeof this.form === "function") {
        return this.form()
      }
    },
  },

  watch: {
    value: {
      handler: function (newValue) {
        if (! this.field) {
          this.internalValue = newValue
        }
      },
      immediate: true,
    },
    '__form': {
      handler: function (newData) {
        if (newData) {
          this.internalValue = newData[this.field]
        }
      },
      immediate: true,
      deep: true
    }
  },

};

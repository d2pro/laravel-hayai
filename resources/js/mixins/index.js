import FormMixin from './FormMixin'
import PageMixin from './PageMixin'

export {
    FormMixin as form,
    PageMixin as page,
}
/**
 * PageMixin
 *
 * Props (injected from backend by Inertia):
 *  auth
 *  flash_mesage
 *
 * Fires the Flash component if there are a flash message
 * from the backend
 */
export default {
  props: {
    auth: Object,
    flash_message: Object,
  },

  watch: {
    flash_message: {
      immediate: true,
      handler(flash) {
        if (flash) {
          if (flash.type === 'error') {
            this.$error(flash.message)
          } else if (flash.type === 'warning') {
            this.$warning(flash.message)
          } else if (flash.type === 'info') {
            this.$info(flash.message)
          } else {
            this.$success(flash.message)
          }
        }
      }
    }
  },
};

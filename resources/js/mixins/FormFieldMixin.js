/**
 * Mixin para componentes tipo campo que tengan una Label y otro
 * componente donde se hace el input: p.e. InputField
 */

export default {
  inheritAttrs: false,

  props: {
    id: {
      type: String,
      default() {
        return this.$options._componentTag + '-' + this._uid
      }
    },
    label: String,
    field: String,
    value: [String, Number, Boolean, Array, Object],
    errors: { type: Array, default: () => [] },
    errorMessage: String,
  },

  computed: {
    listeners() {
      const { input, ...listeners } = this.$listeners;
      return listeners;
    },
  },

  mounted() {

  },

  methods: {
    onInput(value) {
      this.$emit('input', value)
    }
  },
};

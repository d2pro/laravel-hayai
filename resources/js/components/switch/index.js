import Switch from './Switch'
import SwitchField from './SwitchField'

import { use, registerComponent } from '../../utils/plugins'

const Plugin = {
    install(Vue) {
        registerComponent(Vue, Switch)
        registerComponent(Vue, SwitchField)
    }
}

use(Plugin)

export default Plugin

export {
    Switch as HSwitch,
    SwitchField as HSwitchField,
}
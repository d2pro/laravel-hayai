import Modal from './Modal'
import ConfirmModal from './ConfirmModal'

import { VueInstance } from '../../utils/config'
import { merge } from '../../utils/helpers'
import { use, registerComponent, registerComponentProgrammatic } from '../../utils/plugins'

let localVueInstance

const ModalProgrammatic = {
  confirm(message, customConfig) {
    // Config
    var params = {}
    if (typeof message === 'string') {
      params = {
        message: message
      }
    }
    var componentConfig = merge(params, customConfig)

    // Vue instance
    const vm = typeof window !== 'undefined' && window.Vue ? window.Vue : localVueInstance || VueInstance
    // Component
    const ConfirmModalComponent = vm.extend(ConfirmModal)
    var instance = new ConfirmModalComponent({
      propsData: {
        config: componentConfig,
        programmatic: true,
        autoDestroy: true
      }
    }).$mount()
    // Mount + add + show
    document.querySelector('body').appendChild(instance.$el)
    instance.show()
  },
}

const Plugin = {
  install(Vue) {
    localVueInstance = Vue
    registerComponent(Vue, Modal)
    registerComponent(Vue, ConfirmModal)
    registerComponentProgrammatic(Vue, 'modal', ModalProgrammatic)
  }
}

use(Plugin)

export default Plugin

export {
  ModalProgrammatic,
  Modal as HModal,
  ConfirmModal as HConfirmModal,
}

import Input from './Input'
import InputField from './InputField'

import { use, registerComponent } from '../../utils/plugins'

const Plugin = {
    install(Vue) {
        registerComponent(Vue, Input)
        registerComponent(Vue, InputField)
    }
}

use(Plugin)

export default Plugin

export {
    Input as HInput,
    InputField as HInputField
}
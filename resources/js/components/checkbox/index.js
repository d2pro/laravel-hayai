import Checkbox from './Checkbox'
import CheckboxField from './CheckboxField'

import { use, registerComponent } from '../../utils/plugins'

const Plugin = {
    install(Vue) {
        registerComponent(Vue, Checkbox)
        registerComponent(Vue, CheckboxField)
    }
}

use(Plugin)

export default Plugin

export {
    Checkbox as HCheckbox,
    CheckboxField as HCheckboxField,
}
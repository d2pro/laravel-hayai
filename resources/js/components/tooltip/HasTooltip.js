/**
 * For components that will show tooltips
 */
import TooltipWrapper from './TooltipWrapper'
import { VueInstance } from '../../utils/config'

export default {
  name: 'HasTooltip',

  props: {
    tt: String,
    ttColor: String,
    ttPosition: String
  },

  data() {
    return {
      wrapper: null
    }
  },

  watch: {
    tt: {
      handler() {
        this.computeConfig()
      },
      immediate: true,
    },
    ttColor: {
      handler() {
        this.computeConfig()
      },
      immediate: true,
    },
    ttPosition: {
      handler() {
        this.computeConfig()
      },
      immediate: true,
    },
  },

  mounted() {
    if (! this.tt) {
      return
    }

    // Con esto tomamos la instancia correcta de Vue
    const vm = typeof window !== 'undefined' && window.Vue ? window.Vue : VueInstance
    // Creo el control 'fake' donde aparecerá el TT
    let tooltipWrapper = vm.extend(TooltipWrapper)
    // Se crea la instancia y se monta (poniendo este comp como parent)
    this.wrapper = new tooltipWrapper({
      parent: this
    }).$mount()
    // Añadimos clase 'relative' al parent
    this.$el.classList.add('relative')
    // Añadimos el wrapper al DOM
    this.$el.appendChild(this.wrapper.$el)
    // this.$el.prepend(this.wrapper.$el)
  },

  methods: {
    computeConfig() {
      var ttConfig = {}
      if (this.tt) {
        ttConfig.content = this.tt
        if (this.ttColor) {
          ttConfig.classes = this.ttColor
        }
        if (this.ttPosition) {
          ttConfig.placement = this.ttPosition
        }
        // Debug
        // ttConfig.show = true
        // ttConfig.trigger = 'manual'
      }
      // console.log('tooltipComputed', ttConfig)
      if (this.wrapper) {
        this.wrapper.ttConfig = ttConfig
      }
    }
  }
}

/**
 * Mixin to be included in the Custom Modals for edit resources
 * It's included automatically before mount the custom modal
 */
export default {
  props: {
    parent: Object,
  },

  data() {
    return {
      open: false,
      instance: {},
    }
  },

  watch: {
    parent: {
      handler(newData) {
        this.open = newData.openModal
        this.instance = newData.editingInstance
      },
      immediate: true,
      deep: true,
    },
  },

  methods: {
    close() {
      this.open = false
    },
  }
}

import { InertiaApp } from '@inertiajs/inertia-vue'
import { vueInstance, registerCollection } from '../utils/plugins'

export default {
  InertiaApp,

  render: h => h(InertiaApp, {
    props: {
      initialPage: JSON.parse(app.dataset.page),
      resolveComponent: name => {
        let parts = name.split('/')
        if (parts[0] === '_hayai') {
          // Componente HAYAI (de página)
          return import(`../views/${parts[1]}`).then(module => module.default)
        }
        // Página normal
        return import(`@/Pages/${name}`).then(module => module.default)
      },
    },
  }),

  /**
   * Para registrar los distintos layouts de la aplicación que use Hayai
   */
  registerLayout(name, layout) {
    let Vue = vueInstance()
    registerCollection(Vue, '_layouts', name, layout)
  },
}
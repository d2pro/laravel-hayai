import axios from 'axios'

export const setup = (Vue) => {
  // Sets the user's browser language
  let lang = navigator.language.split('-')[0]
  axios.defaults.headers.common['Accept-Language'] = lang

  // Authorization
  axios.defaults.withCredentials = true

  // Error 422: Validation handling
  let api = axios.create()
  api.interceptors.response.use(
    response => response,
    error => {
      if (error.response && error.response.status === 422) {
        if (Vue.prototype.$page) {
          Vue.prototype.$page.errors = error.response.data.errors
        } else {
          console.log('Intertia $page object it\'s not accesible')
        }
        return Promise.resolve()
      } else {
        return Promise.reject(error)
      }
    }
  )

  // Make global
  Vue.prototype.$http = api
}

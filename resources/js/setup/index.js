import Inertia from './InertiaSetup'
import { setup } from './AxiosSetup'

export {
    Inertia,
    setup as AxiosSetup,
}
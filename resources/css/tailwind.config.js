const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  theme: {
    extend: {
      colors: {
        brand: {
          '100': 'var(--brand-100)',
          '200': 'var(--brand-200)',
          '300': 'var(--brand-300)',
          '400': 'var(--brand-400)',
          '500': 'var(--brand-500)',
          '600': 'var(--brand-600)',
          '700': 'var(--brand-700)',
          '800': 'var(--brand-800)',
          '900': 'var(--brand-900)',
        },
        cta: {
          '100': 'var(--cta-100)',
          '200': 'var(--cta-200)',
          '300': 'var(--cta-300)',
          '400': 'var(--cta-400)',
          '500': 'var(--cta-500)',
          '600': 'var(--cta-600)',
          '700': 'var(--cta-700)',
          '800': 'var(--cta-800)',
          '900': 'var(--cta-900)',
        },
      },
      fontFamily: {
        sans: [
          'var(--font-family-sans)',
          ...defaultTheme.fontFamily.sans
        ],
        serif: [
          'var(--font-family-serif)',
          ...defaultTheme.fontFamily.serif
        ]
      },
      width: {
        '14': '3.5rem',
      },
      borderWidth: {
        '3': '3px',
        '5': '5px',
      },
      borderRadius: {
        'xl': '2rem'
      },
    },
    fill: global.Object.assign({ 'current': 'currentColor' }, defaultTheme.colors),
  },
  variants: {
    textColor: ['responsive', 'hover', 'focus', 'group-hover'],
    textDecoration: ['responsive', 'hover', 'focus', 'group-hover'],
    fill: ['group-hover'],
    borderWidth: ['responsive', 'hover'],
  },
  plugins: [
    require('tailwind-css-variables')(
      {
        // modules
      },
      {
        // options
        postcssEachVariables: true // default: false
      }
    )
  ]
}

# HAYAI framework documentation

**HAYAI** es un paquete de `Laravel + Vue` que pretende servir como _framework_ para el desarrollo rápido de aplicaciones usando el _stack_ `Laravel + Vue.js + Inertia.js + TailwindCSS`.

La idea es incluir en este paquete todo lo que sea útil con la finalidad de poder desarrollar de forma rápida, eficiente y homogénea aplicaciones. Por la tanto, **HAYAI** incluye elementos tanto de `PHP` (controladores, _middlewares_, _providers_, clases, recursos, _helpers_, etc.) como de `Vue (js)` (componentes, páginas, _mixins_, _helpers_, etc.) programados con los principios **_SOLID_** de desarrollo de _software_, para que las aplicaciones desarrolladas sean homogéneas (mantengan una estética y una funcionalidad coherente), eficientes (que se basen en componentes bien programados y testados ampliamente), mantenibles (es decir, fácilmente mantenibles) y que implementen un conjunto de buenas prácticas y técnicas de programación.

Este último aspecto tiene la ventaja, además, de obligarnos a los programadores a implementar ese conjunto de técnicas y buenas prácticas de programación.

**HAYAI** se centra principalmente en eliminar todo lo posible la repetición de los mismos procesos que hay que desarrollar en cada aplicación. Por ejemplo, operaciones tipo `CRUD`, serán mucho más fácil y rápido de implementar.

La combinación de **HAYAI** como _framework_ base para todas las aplicaciones que desarrolle **D2PRO**, junto con paquetes creados para dar solución a diferentes funcionalidades requeridas en los distintos proyectos (como facturación, pagos, gestión documental, etc.) permitirá que éstas alcancen los principios expuestos de homogeneidad y rapidez de desarrollo.

    Este paquete permanecerá privado y estará reservado al uso interno en los proyectos de **D2PRO**.


### ¿De dónde viene el nombre de _HAYAI_?

_Hayai_ significa _'veloz'_ en japonés (早い)


### Y, ¿qué pasa con _Lavutain_?

**Lavutain** pasará a ser nuestro esqueleto de app `Laravel` por defecto. Es decir será el principio de nuestro desarrollo de una app. Por supuesto, integrará el paquete **HAYAI**, así como algunos otros de creación propia o externos que vayamos a usar en casi todas nuestras apps.

La mayoría de, si no todos, los componentes de `Vue` se irán pasando e integrando poco a poco en **HAYAI**.


#### ¿Quiere decir esto que sólo podrá usarse HAYAI con Lavutain?

¡No! **HAYAI** será un paquete de `Laravel` independiente que podrá integrarse en otras apps que lo requieran.


#### ¿Cuándo debo usar Lavutain o HAYAI integrado en otra app?

Fácil: por defecto siempre iniciaremos un proyecto nuevo usando **Lavutain**, a no ser que sea un proyecto muy especial o con características _"raras"_ que hagan más viable iniciarlo con un esqueleto de `Laravel` estándar.

# Recursos

Un recurso hace referencia a un modelo de datos o tabla dentro de una base de datos. Normalmente un recurso será un modelo `Eloquent`, pero también puede hacer referencia a un `DTO`[^1].

Además cuando en **HAYAI** hablamos de _recurso_ o `resource` hacemos referencia también a las operaciones `CRUD` típicas que hay que hacer con el modelo: `index`, `create`, `store`, `edit`, `update`, `delete` y `restore`.

Para la gestión de un `recurso` **HAYAI** incluye tanto la parte de _backend_ (`requests`, validaciones, operaciones en BD, etc.) como la de _frontend_ (vistas y componentes de `Vue`).



[^1]: Data Transfer Object
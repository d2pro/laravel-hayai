# Instalación

_**NOTA**: tienes que tener, mínimo, acceso como programador al repositorio para poder instalar **HAYAI**._

Para instalar **HAYAI** como un paquete normal en una aplicación `Laravel` ([una vez que ya tenemos creada dicha app](https://laravel.com/docs/7.x)), debemos seguir los siguientes pasos:

## 1. Añadir referencia al repositorio de HAYAI en el `composer.json`

Para ello tenemos que añadir la siguente entrada al fichero `composer.json` de nuestra app:

```json
"repositories": {
    "laravel-hayai": {
        "type": "vcs",
        "url": "git@bitbucket.org:d2pro/laravel-hayai.git"
    }
}
```

Después debemos ejecutar en nuestro terminal:

```bash
$ composer require d2pro/laravel-hayai
```


Como esto lo tendremos que repetir más de una vez, podemos añadir la siguiente función `bash` a nuestro fichero `~/.bashrc`, `~/.bash_profile` o `~/.zshrc`:

```bash
composer-hayai() {
  branchName=dev-${1:-master}

  composer config repositories.hayai '{"type": "vcs", "url": "git@bitbucket.org:d2pro/laravel-hayai.git"}' --file composer.json
  composer require d2pro/laravel-hayai:$branchName
}
```

Una vez que hayamos recargado el fichero donde hayamos añadido la función, podemos ejecutar el comando `composer-hayai develop` para añadir e instalar la rama `develop` de **HAYAI** a nuestro proyecto. Si, en cambio, preferimos la rama `master` simplemente hacemos `composer-hayai` o `composer-hayai master`.

## 2. Publicamos los recursos y la configuración

A continuación ejecutamos:

```bash
$ php artisan hayai:install
```

Esto publicará el fichero de configuración de **HAYAI** (`config/hayai.php`), con el que podremos adaptar algunas configuraciones a los requerimientos de nuestro proyecto actual.

Si estamos usando la **estructura de dominios** en nuestra app (como en **Lavutain**), podemos indicarlo en el comando de la siguiente forma:

```bash
$ php artisan hayai:install --app-domain
```

Esto modificará la opción `searchByAppDomain` del fichero de configuración para indicarle a **HAYAI** dónde ha de buscar la ubicación de las distintas clases.

Con el modificador `--force` podemos forzar a que se generen de nuevo los ficheros de la instalación, <ins>**pero esto sobreescribirá los ficheros anteriores y perderemos cualquier cambio que hayamos hecho a los mismos**</ins>.

## 3. Revisar CSS / Tailwind
## 4. yarn && yarn dev

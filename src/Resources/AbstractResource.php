<?php

namespace D2PRO\Hayai\Resources;

use D2PRO\Hayai\Fields\Field;
use D2PRO\Hayai\Requests\HayaiRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Inertia\Inertia;

abstract class AbstractResource
{
    use ResolveFieldsTrait, SerializeResourceTrait;

    /**
     * Vue component to render the Index
     */
    private const VUE_COMPONENT = 'ResourceIndex';

    /**
     * Indicates if the resource must use modals for create and edit forms
     */
    public bool $useModalForEdition = false;

    /**
     * The resource title (for display purposes)
     */
    public static string $resourceTitle = '';

    /**
     * The resource plural title (for display purposes)
     * If blank, Str::plural($resourceTitle) will be used
     */
    public static string $resourceTitlePlural = '';

    /**
     * Indicates the resource title is plural
     * (used to accomodate the right pronoums in Spanish and other languages)
     */
    public bool $resourceTitleIsFemale = false;

    /**
     * The Page title for the resource
     * If blank, $resourceTitle will be used
     */
    public string $pageTitle = '';

    /**
     * The Header title for the resource
     * If blank, $resourceTitle will be used
     */
    public string $headerTitle = '';

    /**
     * The base route name for the resource
     */
    public static string $routeName = '';

    /**
     * The base route path for the resource
     * If blank, the path will be the route name sluged
     * (The dots '.' will be replaced wiht '/')
     */
    public static string $routePath = '';

    /**
     * The route middleware. Auth is the default but can be modified
     * per Resource.
     * This middleware is used to register the routes for the resource:
     * index, create, store, edit, update, delete, restore
     */
    public static string $routeMiddleware = 'auth';

    /**
     * The resource page layout in Vue
     * So there must be a 'AppLayout.vue' file in Vue resources 'Layouts' folder
     */
    public string $frontLayout = 'AppLayout';

    /**
     * Indicates if the filter component will be show in the
     * index page of the resource
     */
    public bool $showFilterComponent = true;

    /**
     * Indicates if the search box will be shown in the index page of the resource
     */
    public bool $showSearch = true;

    /**
     * Indicates if the resource allows to create new models
     */
    public bool $allowCreate = true;

    /**
     * Indicates if the resource allows to edit the models
     */
    public bool $allowEdit = true;

    /**
     * Indicates if the resource allows to delete models
     */
    public bool $allowDelete = true;

    /**
     * Indicates if the resource allows to restore deleted models
     */
    public bool $allowRestore = true;

    /**
     * Set the modal component to be used for editions
     * (i.e. '@\Pages\Components\CustomModal.vue')
     * default false
     */
    public $useCustomModalForEdition = false;

    /**
     * The column used to 'name' a specific model instance or row
     */
    public string $nameColumn = 'name';

    /**
     * Paginate rows for index?
     */
    protected bool $paginate = true;

    /**
     * The current number of rows to paginate
     */
    protected int $paginateRows = 20;

    /**
     * The options to select the number of pagination rows
     */
    protected array $paginateOptions = [10, 25, 50];

    /**
     * Pagination links to send to front
     */
    protected array $paginateLinks;

    /**
     * The request
     */
    protected HayaiRequest $request;

    /**
     * This must be implemented in the child resource and will contains
     * all the field definitions in an array
     *
     * @return array
     */
    abstract protected function fields(): array;

    /**
     * The fields that must be shown in the index page of the resource
     * (optional)
     */
    protected function fieldsForIndex(): array
    {
        return [];
    }

    /**
     * The columns that should be searched
     *
     * @var array
     */
    public static $searchColumns = [];

    /**
     * Constructor
     */
    public function __construct(HayaiRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Used to initialize a new instance of the resource
     * loading the data
     */
    public static function renderForIndex(HayaiRequest $request)
    {
        // Instantiate the child class, not this abstract class :)
        $childClass = get_called_class();
        $resource = new $childClass($request);

        // Render the page
        return Inertia::render(
            '_hayai/'.self::VUE_COMPONENT,
            ['_hayai' => $resource->serializeForIndex()]
        );
    }

    /**
     * Prepares the data (every row) before send it to the index page
     */
    public function collectDataForIndex(): array
    {
        $data = $this->getDataForIndex();
        // dd($data);
        $dataForIndex = [];

        foreach ($data as $instance) {
            $row = [];

            // Process fields for display format
            $fields = $this->getFieldsForIndex()->merge($this->getFieldsForEdit())->filter();
            foreach ($fields as $field) {
                // Comprobar aquí tipos de campos que nunca vayan al index
                // Evalutate if the field must be shown or hide
                $hide = $field->hideFromIndex;
                if (is_callable($field->hideFromIndexCallback)) {
                    $hide = call_user_func($field->hideFromIndexCallback, $this);
                } elseif (is_callable($field->showInIndexCallback)) {
                    $hide = ! call_user_func($field->showInIndexCallback, $this);
                }

                if (! $hide) {
                    $field->formatForIndex($instance);
                    $row[$field->tableField()] = $field->value;
                }
            }

            // Can be this instance deleted?
            if ($this->allowDelete) {
                $row['_canBeDeleted'] = static::canBeDeleted($instance);
            }

            $dataForIndex[] = $row;
        }

        return $dataForIndex;
    }

    /**
     * Abstract method to get the data for the index
     */
    abstract public function getDataForIndex(): Collection;

    /**
     * Abstract method to be implemented for store new model or row
     *
     * @return array A tupple with (success, message)
     */
    abstract public function _store(): array;

    /**
     * Validates the request data with the corresponding field rules
     */
    protected function validate(string $method = 'Creation')
    {
        $fieldsToValidate = [];
        $rules = [];
        foreach ($this->fields() as $field) {
            $fieldRules = $field->{'get'.$method.'Rules'}();
            if ($fieldRules) {
                $fieldsToValidate[] = $field->tableField();
                $rules[$field->tableField()] = $this->_resolveRules($fieldRules);
            }
        }

        return $this->request->validate($rules);
    }

    /**
     * Find all the {field} matches in a rule and replace it
     * with the corresponding field value from the request
     */
    private function _resolveRules(array $rules): array
    {
        $newRules = [];

        foreach ($rules as $rule) {
            $matches = [];
            $results = preg_match_all('/{(.*?)}/', $rule, $matches);
            for ($i = 0; $i < $results; $i++) {
                $rule = str_replace($matches[0][$i], $this->request->input($matches[1][$i]), $rule);
            }
            // dd('rules', $rule, $results, $matches);
            $newRules[] = $rule;
        }

        return $newRules;
    }

    /**
     * Gets the appropiate message after a store/create operation
     *
     * TODO: translate
     */
    protected function _storeResultMessage(bool $success): string
    {
        if ($success) {
            return ($this->resourceTitleIsFemale ? 'La ' : 'El ').'<strong>'.
                strtolower(static::$resourceTitle).'</strong> se ha creado correctamente.';
        } else {
            return '¡No se ha podido crear '.($this->resourceTitleIsFemale ? 'la ' : 'el nuevo ').'<strong>'.
                strtolower(static::$resourceTitle).'</strong>!';
        }
    }

    /**
     * Abstract method to be implemented for update a model or row
     *
     * @return array A tupple with (success, message)
     */
    abstract public function _update(int $resourceId): array;

    /**
     * Gets the appropiate message after a update/edit operation
     *
     * TODO: translate
     */
    protected function _updateResultMessage(bool $success): string
    {
        if ($success) {
            return ($this->resourceTitleIsFemale ? 'La ' : 'El ').'<strong>'.
                strtolower(static::$resourceTitle).'</strong> se ha actualizado correctamente.';
        } else {
            return '¡No se ha podido actualizar '.($this->resourceTitleIsFemale ? 'la ' : 'el ').'<strong>'.
                strtolower(static::$resourceTitle).'</strong>!';
        }
    }

    /**
     * Abstract method to be implemented for deleted a model or row
     *
     * @return array A tupple with (success, message)
     */
    abstract public function _delete(int $resourceId): array;

    /**
     * Gets the appropiate message after a delete operation
     *
     * TODO: translate
     */
    protected function _deleteResultMessage(bool $success): string
    {
        if ($success) {
            return ($this->resourceTitleIsFemale ? 'La ' : 'El ').'<strong>'.
                strtolower(static::$resourceTitle).'</strong> ha sido borrad'.
                 ($this->resourceTitleIsFemale ? 'a' : 'o').' correctamente.';
        } else {
            return '¡No se ha podido eliminar '.($this->resourceTitleIsFemale ? 'la ' : 'el ').'<strong>'.
                strtolower(static::$resourceTitle).'</strong>!';
        }
    }

    /**
     * Should be implemented in the child class to indicate if a particular
     * instance (model or row) can be deleted
     */
    protected static function canBeDeleted($instance): bool
    {
        return true;
    }

    protected function getFieldsForIndex(): Collection
    {
        $indexFields = $this->fieldsForIndex();
        // Si no se han especificado campos concretos para el índice
        // fallback a los campos 'normales'
        if (count($indexFields) === 0) {
            $indexFields = $this->fields();
        }

        return collect($indexFields);
    }

    /**
     * Search in the Index Fields array for
     */
    protected function getIndexFieldByTableField(string $tableField): ?Field
    {
        return
            Arr::first(
                $this->getFieldsForIndex(),
                function ($field) use ($tableField) {
                    return $field->tableField() === $tableField;
                }
            );
    }

    protected function getFieldsForEdit(): array
    {
        // $editFields = $this->fieldsForEdit();
        // Si no se han especificado campos concretos para el índice
        // fallback a los campos 'normales'
        // if (count($editFields) === 0) {
        $editFields = $this->fields();
        // }

        return $editFields;
    }

    public static function getResourceTitle(): string
    {
        if (! static::$resourceTitle) {
            static::$resourceTitle = static::_resourceTitleFromChildClassName();
        }

        return Str::title(static::$resourceTitle);
    }

    public static function getResourceTitlePlural(): string
    {
        if (! static::$resourceTitlePlural) {
            static::$resourceTitlePlural = Str::plural(static::getResourceTitle());
        }

        return static::$resourceTitlePlural;
    }

    public static function getRouteName(): string
    {
        if (! static::$routeName) {
            return Str::lower(static::getResourceTitlePlural());
        }

        return static::$routeName;
    }

    public static function getRoutePath(): string
    {
        if (! static::$routePath) {
            return str_replace('.', '/', static::getRouteName());
        }

        return static::$routePath;
    }

    /**
     * Gets the resourceTitle from the class name
     */
    private static function _resourceTitleFromChildClassName(): string
    {
        $childClass = get_called_class();
        $parts = explode('\\', $childClass);
        $childClass = end($parts);
        if (Str::endsWith($childClass, 'Resource')) {
            $childClass = Str::replaceLast('Resource', '', $childClass);
        }

        return $childClass;
    }
}

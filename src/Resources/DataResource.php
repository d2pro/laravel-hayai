<?php

namespace D2PRO\Hayai\Resources;

/**
 * DataResource
 * Use this only for resources not based in an Eloquent model
 */
abstract class DataResource extends AbstractResource
{
    /***************************************************************
     *
     * Implementation of the abstract methods from AbstractResource
     *
     ***************************************************************/

    protected function search()
    {
        if ($search = $this->request->input('search')) {
            $filtered = $this->data->filter(function ($row) use ($search) {
                $filter = false;
                foreach ($this->fields() as $field) {
                    if ($field->isSearchable()) {
                        $filter = $filter
                            || (
                                stripos(
                                    $row->{$field->tableField()},
                                    $search
                                ) !== false
                            );
                    }
                }
                // dd($filter);
                return $filter;
            });
            $this->data = $filtered;
        }
    }

    protected function sort()
    {
        if ($sort = $this->request->input('sort')) {
            $parts = explode(',', $sort);
            if (count($parts) === 2) {
                $sorted = null;
                if ($parts[1] === 'asc') {
                    $sorted = $this->data->sortBy($parts[0]);
                } else {
                    $sorted = $this->data->sortByDesc($parts[0]);
                }
                $this->data = $sorted;
            }
        }
    }

    /**
     * Make the store action with the validated data
     */
    abstract protected function storeWithData(array $data): bool;

    public function _store(): array
    {
        $success = $this->storeWithData($this->validate());

        return [
            $success,
            $this->_storeResultMessage($success),
        ];
    }

    /**
     * Make the store action with the validated data
     */
    abstract protected function updateWithData(int $resourceId, array $data): bool;

    public function _update(int $resourceId): array
    {
        $success = $this->updateWithData($resourceId, $this->validate($request, 'Update'));

        return [
            $success,
            $this->_updateResultMessage($success),
        ];
    }

    /**
     * Make the delete action
     */
    abstract protected function deleteWithId(int $resourceId): bool;

    public function _delete(int $resourceId): array
    {
        $success = $this->deleteWithId($resourceId);

        return [
            $success,
            $this->_deleteResultMessage($success),
        ];
    }
}

<?php

namespace D2PRO\Hayai\Resources;

use D2PRO\Hayai\Fields\FieldCollection;

trait ResolveFieldsTrait
{
    /**
     * Resolves the fields for index page
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return \D2PRO\Hayai\Fields\FieldCollection
     */
    public function indexFields(NovaRequest $request)
    {
        return $this->resolveFields($request, function (FieldCollection $fields) use ($request) {
            return $fields->reject(function ($field) use ($request) {
                return $field instanceof ListableField || ! $field->isShownOnIndex($request, $this->resource);
            });
        })->each(function ($field) use ($request) {
            if ($field instanceof Resolvable && ! $field->pivot) {
                $field->resolveForDisplay($this->resource);
            }

            if ($field instanceof Resolvable && $field->pivot) {
                $accessor = $this->pivotAccessorFor($request, $request->viaResource);

                $field->resolveForDisplay($this->{$accessor} ?? new Pivot);
            }
        });
    }
}

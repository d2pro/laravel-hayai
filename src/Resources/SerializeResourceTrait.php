<?php

namespace D2PRO\Hayai\Resources;

use Illuminate\Support\Collection;

trait SerializeResourceTrait
{
    /**
     * Collection of the fields serialized for Index
     */

    private Collection $_fieldsForIndex;
    /**
     * Collection of the fields serialized for Edit
     */
    private Collection $_fieldsForEdit;

    /**
     * Generate JSON to send to the front-end with all the
     * config for the resource
     * @return array (to be converted in JSON)
     */
    protected function serializeForIndex(): array
    {
        $array = $this->_serializeResource();

        // Actions allowed/forbidden
        $array['actions'] = [
            'filter' => $this->showFilterComponent,
            'search' => $this->showSearch,
            'create' => $this->allowCreate,
            'edit' => $this->allowEdit,
            'delete' => $this->allowDelete,
            'restore' => $this->allowRestore,
        ];

        $array['fields'] = $this->_serializesFieldsForIndex();

        if ($this->useModalForEdition) {
            $array['editFields'] = $this->_serializesFieldsForEdit();
        }

        $array['rows'] = $this->collectDataForIndex();
        $array['paginationLinks'] = $this->paginateLinks ?? [];
        $array['filters'] = [
            'search' => $this->request->searchQuery,
            'sort' => $this->request->sortField
                ? $this->request->sortField.'|'.$this->request->sortDirection
                : null,
        ];

        return $array;
    }

    /**
     * Serialize main resource attributes
     */
    private function _serializeResource(): array
    {
        return [
            'titles' => [
                'page' => $this->pageTitle ?? static::$resourceTitlePlural,
                'header' => $this->headerTitle ?? static::$resourceTitlePlural,
                'resource' => static::getResourceTitle(),
                'resourcePlural' => static::getResourceTitlePlural(),
                'resourceIsFemale' => $this->resourceTitleIsFemale,
            ],
            'route' => static::getRouteName(),
            'layout' => $this->frontLayout,
            'nameColumn' => $this->nameColumn,
            'customModalForEdition' => $this->useCustomModalForEdition,
        ];
    }

    /**
     * Generate JSON for each field to send its definition to the front-end
     *
     * @return array
     */
    private function _serializesFieldsForIndex(): array
    {
        $this->_fieldsForIndex = new Collection();

        $fields = [];

        foreach ($this->getFieldsForIndex() as $field) {
            // Evalutate if the field must be shown or hide
            $hide = $field->hideFromIndex;
            if (is_callable($field->hideFromIndexCallback)) {
                $hide = call_user_func($field->hideFromIndexCallback, $this);
            } elseif (is_callable($field->showInIndexCallback)) {
                $hide = ! call_user_func($field->showInIndexCallback, $this);
            }
            if (! $hide) {
                $fields[] = $field->toFront();
            }
        }

        return $fields;
    }

    private function _serializesFieldsForEdit(): array
    {
        $fields = [];

        foreach ($this->getFieldsForEdit() as $field) {
            $addField = $field->isEditable();
            if (is_callable($field->allowViewCallback)) {
                $addField &= call_user_func($field->allowViewCallback, $this);
                // dd($field, call_user_func($field->allowViewCallback, $this));
            }
            if ($addField) {
                $fields[] = $field->toFront();
            }
        }

        return $fields;
    }
}

<?php

namespace D2PRO\Hayai\Resources;

use D2PRO\Hayai\Fields\Field;
use D2PRO\Hayai\HayaiPaginator;
use D2PRO\Hayai\Requests\HayaiRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

abstract class ModelResource extends AbstractResource
{
    /**
     * Model Class for the resource
     * @var string
     */
    public static $model;

    public function __construct(HayaiRequest $request)
    {
        // Checks if $model has been declared in child
        if (! static::$model) {
            throw new \Exception('[Hayai]: resource model is empty!');
        }

        parent::__construct($request);
    }

    /***************************************************************
     *
     * Implementation of the abstract methods from AbstractResource
     *
     ***************************************************************/

    public function getDataForIndex(): Collection
    {
        // indexQuery -> searchQuery -> sortQuery
        $query = $this->sortQuery(
            $this->searchQuery(
                $this->indexQuery(),
                $this->request->searchQuery
            ),
            $this->request->sortField,
            $this->request->sortDirection
        );

        // ...finally -> paginate
        return $this->paginate($query);
    }

    /**
     * The query builder for index
     */
    protected function indexQuery(): Builder
    {
        return (new static::$model)->newQuery();
    }

    /**
     * The query builder to make searches in rows
     */
    protected function searchQuery(Builder $query, ?string $searchQuery)
    {
        if ($searchQuery) {
            $fields = $this->getFieldsForIndex();
            $fields->each(function (Field $field) use ($query, $searchQuery) {
                // Campo 'searcheable'
                if ($field->isSearchable()) {
                    // Miramos sh hay un callback definido para este campo
                    if (is_callable($field->searchByCallback)) {
                        $query = call_user_func($field->searchByCallback, $query, $searchQuery);
                    } else {
                        $query->orWhere($field->tableField(), 'LIKE', '%'.$searchQuery.'%');
                    }
                }
            });
        }

        return $query;
    }

    /**
     * The query to sort the rows
     */
    protected function sortQuery(Builder $query, ?string $sortField, ?string $sortDirection)
    {
        if ($sortField) {
            // Buscamos la definición del campo para ver si se ha especificado
            $indexField = $this->getIndexFieldByTableField($sortField);
            if ($indexField && $indexField->isSortable()) {
                // Miramos si hay un callback 'sortBy' para ordenarlo
                if ($indexField && is_callable($indexField->sortByCallback)) {
                    $query = call_user_func($indexField->sortByCallback, $query, $sortDirection);
                } else {
                    $query->orderBy($sortField, $sortDirection);
                }
            }
        }

        return $query;
    }

    /**
     * Make the pagination of the rows
     */
    protected function paginate(Builder $query): Collection
    {
        if ($this->paginate) {
            $paginator = new HayaiPaginator($this->request, $query->applyScopes(), $this->paginateRows);
            $this->paginateLinks = $paginator->links();
            $data = $paginator->paginate();

            return $data;
        }

        return $query->get();
    }

    /**
     * You must override this method if need to do extra things
     */
    protected function storeAction(): bool
    {
        return static::$model::create($this->validate()) ? true : false;
    }

    /**
     * Make the store action with the validated data
     */
    public function _store(): array
    {
        $success = $this->storeAction();

        return [
            $success,
            $this->_storeResultMessage($success),
        ];
    }

    /**
     * You must override this method if need to do extra things
     */
    protected function updateAction($instance): bool
    {
        return $instance->update($this->validate('update')) ? true : false;
    }

    /**
     * Make the store action with the validated data
     */
    public function _update(int $resourceId): array
    {
        $instance = static::$model::findOrFail($resourceId);

        $success = $this->updateAction($instance);

        return [
            $success,
            $this->_updateResultMessage($success),
        ];
    }

    /**
     * You must override this method if need to do extra things
     */
    protected function deleteAction($instance): bool
    {
        return $instance->delete() ? true : false;
    }

    /**
     * Make the delete action
     */
    public function _delete(int $resourceId): array
    {
        $instance = static::$model::findOrFail($resourceId);

        $success = $this->deleteAction($instance);

        return [
            $success,
            $this->_deleteResultMessage($success),
        ];
    }
}

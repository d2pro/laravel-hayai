<?php

namespace D2PRO\Hayai;

use D2PRO\Hayai\Resources\AbstractResource;
use Illuminate\Support\Facades\Route;
use ReflectionClass;

class Hayai
{
    /**
     * The registered resource names.
     */
    public static array $resources = [];

    /**
     * List of resource names by its model names.
     */
    // public static array $resourcesByModel = [];

    /**
     * Current HAYAI version.
     */
    public static function version(): string
    {
        return '0.0.1';
    }

    /**
     * Register the Hayai resources in App\Resources
     * or in case this is an app-domain-folder-structure app,
     * then in every App\{MyApp}\Resources
     */
    public static function registerResources()
    {
        $useAppDomain = config('hayai.resources.searchByAppDomain');

        // Si es con app/dominio buscamos en cada carpeta de app...
        // app/App/Admin, app/App/App1, etc.
        $searchFolders = $useAppDomain ?
            glob(app_path().'/*', GLOB_ONLYDIR) :
            [app_path()];

        // Lista de los ficheros de clase '...Resource.php'
        $files = [];
        foreach ($searchFolders as $folder) {
            $files = array_merge(
                $files,
                glob($folder.DIRECTORY_SEPARATOR.'Resources'.DIRECTORY_SEPARATOR.'*Resource.php')
            );
        }

        // Registra cada Resource
        $resources = [];

        foreach ($files as $filePath) {
            if ($resource = static::registerResourceFromFile($filePath)) {
                $resources[] = $resource;
            }
        }

        static::saveResources(
            collect($resources)->sort()->all()
        );
    }

    /**
     * Register a Resource from it's PHP file
     */
    private static function registerResourceFromFile(string $filePath): ?String
    {
        list($namespace, $class) = HayaiClassExtractor::extract($filePath);
        $resourceClassName = $namespace.'\\'.$class;

        // Comprobamos que sea un Resource de Hayai
        if (is_subclass_of($resourceClassName, AbstractResource::class) &&
                ! (new ReflectionClass($resourceClassName))->isAbstract()) {
            static::registerResourceRoutes($resourceClassName);

            return $resourceClassName;
        }
    }

    /**
     * Register the routes for the Resource
     * under the default 'web' middleware +
     * the 'auth' middleware declared in the resource
     */
    private static function registerResourceRoutes(string $resourceClassName)
    {
        $routeName = $resourceClassName::getRouteName();
        $routePath = $resourceClassName::getRoutePath();
        $routeMiddleware = $resourceClassName::$routeMiddleware;

        Route::middleware('web')
            ->namespace('\D2PRO\Hayai\Controllers')
            ->group(function () use ($resourceClassName, $routeName, $routePath, $routeMiddleware) {
                // Index
                Route::get($routePath)
                    ->name($routeName)
                    ->middleware($routeMiddleware)
                    ->uses('ResourceIndexController')
                    ->defaults('resourceClass', $resourceClassName);

                // TODO: Create
                // Route::get($routePath.'/create')
                //     ->name($routeName.'.create')
                //     ->uses('ResourceCreateController')
                //     ->defaults('resourceClass', $resourceClassName);

                // Store
                Route::post($routePath.'/store')
                    ->name($routeName.'.store')
                    ->uses('ResourceStoreController')
                    ->defaults('resourceClass', $resourceClassName);

                // TODO: Edit
                // Route::get($routePath.'/{id}/edit')
                //     ->name($routeName.'.edit')
                //     ->uses('ResourceEditController')
                //     ->defaults('resourceClass', $resourceClassName);

                // Update
                Route::put($routePath.'/{id}/edit')
                    ->name($routeName.'.update')
                    ->uses('ResourceUpdateController')
                    ->defaults('resourceClass', $resourceClassName);

                // Delete
                Route::delete($routePath.'/{id}/delete')
                    ->name($routeName.'.delete')
                    ->uses('ResourceDeleteController')
                    ->defaults('resourceClass', $resourceClassName);

                // TODO - Restore
                // Route::put($routePath.'/{id}/restore')
                //     ->name($routeName.'.restore')
                //     ->uses('ResourceRestoreController')
                //     ->defaults('resourceClass', $resourceClassName);
            });
    }

    /**
     * Save (in static memory) the given resources
     *
     * @return static
     */
    private static function saveResources(array $resources)
    {
        static::$resources = array_unique(
            array_merge(static::$resources, $resources)
        );

        return new static;
    }
}

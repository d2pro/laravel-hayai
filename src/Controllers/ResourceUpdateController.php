<?php

namespace D2PRO\Hayai\Controllers;

use D2PRO\Hayai\Requests\HayaiRequest;

class ResourceUpdateController
{
    use ResourceResponserControllerTrait;

    public function __invoke(
        HayaiRequest $request,
        int $resourceId,
        string $resourceClass
    ) {
        $this->initResource($request, $resourceClass);

        return $this->response(
            $request,
            $this->resource->_update($resourceId)
        );
    }
}

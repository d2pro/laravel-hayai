<?php

namespace D2PRO\Hayai\Controllers;

use D2PRO\Hayai\Requests\HayaiRequest;

class ResourceStoreController
{
    use ResourceResponserControllerTrait;

    public function __invoke(
        HayaiRequest $request,
        string $resourceClass
    ) {
        $this->initResource($request, $resourceClass);

        return $this->response(
            $request,
            $this->resource->_store()
        );
    }
}

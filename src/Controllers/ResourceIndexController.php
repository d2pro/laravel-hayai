<?php

namespace D2PRO\Hayai\Controllers;

use D2PRO\Hayai\Requests\HayaiRequest;

class ResourceIndexController
{
    public function __invoke(
        HayaiRequest $request,
        string $resourceClass
    ) {
        return $resourceClass::renderForIndex($request);
    }
}

<?php

namespace D2PRO\Hayai\Controllers;

use D2PRO\Hayai\Requests\HayaiRequest;

class ResourceDeleteController
{
    use ResourceResponserControllerTrait;

    public function __invoke(
        HayaiRequest $request,
        int $resourceId,
        string $resourceClass
    ) {
        $this->initResource($request, $resourceClass);

        return $this->response(
            $request,
            $this->resource->_delete($resourceId),
            false
        );
    }
}

<?php

namespace D2PRO\Hayai\Controllers;

use D2PRO\Hayai\Requests\HayaiRequest;
use D2PRO\Hayai\Resources\AbstractResource;
use Illuminate\Http\Request;

trait ResourceResponserControllerTrait
{
    private AbstractResource $resource;

    private function initResource(HayaiRequest $request, string $resourceClass)
    {
        $this->resource = new $resourceClass($request);
    }

    private function response(Request $request, array $response, $reloadData = true)
    {
        list($success, $message) = $response;

        if ($request->wantsJson()) {
            $json = [
                'success' => $success,
                'message' => $message,
            ];
            if ($reloadData) {
                $json['rows'] = $this->resource->collectDataForIndex();
            }
            return response()->json($json);
        } else {
            return back()
                ->with(
                    'flash',
                    [
                        'type' => $success ? 'success' : 'error',
                        'message' => $message,
                    ]
                );
        }
    }
}

<?php

namespace D2PRO\Hayai;

use D2PRO\Hayai\Requests\HayaiRequest;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class HayaiPaginator
{
    /**
     * Maximun number of link offsets
     */
    private const MAX_SIDE_LINKS = 2;
    private const PAGE_URL_PARAMETER = 'p';

    private HayaiRequest $_request;
    private $_query;

    public int $perPage;
    public int $totalRows;
    public int $totalPages;
    public int $currentPage;

    public function __construct(HayaiRequest $request, $query, int $perPage)
    {
        $this->_request = $request;
        $this->_query = $query;
        $this->perPage = $perPage;

        // Total rows
        $this->totalRows = $this->_query->getQuery()->getCountForPagination();

        // Total pages
        $this->totalPages = max((int) ceil($this->totalRows / $this->perPage), 1);

        // Current page
        $this->currentPage = $this->validPage();
    }

    public function paginate(): Collection
    {
        $offset = (($this->currentPage - 1) * $this->perPage);

        return $this->_query
            ->offset($offset)
            ->limit($this->perPage)
            ->get();
    }

    public function links(): array
    {
        if ($this->totalPages < 2) {
            return [];
        }

        // Prev
        $links = [
            $this->_link('Previous', $this->previousPageUrl(), false),
        ];

        $midLinks = [];

        $maxLinks = self::MAX_SIDE_LINKS + 4;
        if ($this->totalPages <= ((self::MAX_SIDE_LINKS * 2) + 8)) {
            $midLinks = $this->_linkRange(1, $this->totalPages);
        } elseif ($this->currentPage <= $maxLinks) {
            $midLinks = $this->_linkRangeWithDivider(
                $this->_linkRange(1, $maxLinks + self::MAX_SIDE_LINKS),
                $this->_linkFinishRange()
            );
        } elseif ($this->currentPage > ($this->totalPages - $maxLinks)) {
            $midLinks = $this->_linkRangeWithDivider(
                $this->_linkStartRange(),
                $this->_linkRange(
                    $this->totalPages - ($maxLinks + (self::MAX_SIDE_LINKS - 1)),
                    $this->totalPages
                )
            );
        } else {
            $midLinks = $this->_linkRangeWithDivider(
                $this->_linkRangeWithDivider(
                    $this->_linkStartRange(),
                    $this->_linkRange(
                        $this->currentPage - self::MAX_SIDE_LINKS,
                        $this->currentPage + self::MAX_SIDE_LINKS
                    )
                ),
                $this->_linkFinishRange()
            );
        }

        $links = array_merge($links, $midLinks);

        // Next
        $links[] = $this->_link('Next', $this->nextPageUrl(), false);

        return $links;
    }

    private function _linkRangeWithDivider(array $start, array $finish): array
    {
        return array_merge(
            $start,
            [$this->_link('...', null, false)],
            $finish
        );
    }

    private function _linkStartRange(): array
    {
        return $this->_linkRange(1, 2);
    }

    private function _linkFinishRange(): array
    {
        return $this->_linkRange($this->totalPages - 1, $this->totalPages);
    }

    private function _linkRange(int $start, int $end): array
    {
        $links = [];

        for ($i = $start; $i <= $end; $i++) {
            $links[] = $this->_linkForPage($i);
        }

        return $links;
    }

    private function _linkForPage($page)
    {
        return $this->_link(
            $page,
            $this->url($page),
            $page === $this->currentPage
        );
    }

    private function _link($label, $url, $active)
    {
        return [
            'label' => $label,
            'url' => $url,
            'active' => $active,
        ];
    }

    private function previousPageUrl()
    {
        if ($this->currentPage > 1) {
            return $this->url($this->currentPage - 1);
        }
    }

    private function nextPageUrl()
    {
        if ($this->totalPages > $this->currentPage) {
            return $this->url($this->currentPage + 1);
        }
    }

    private function url($page)
    {
        if ($page <= 0) {
            $page = 1;
        }

        // Preserve the paremeters present in the URL: sort, search...
        $parameters = $this->_request->query();
        $parameters = array_merge(
            $parameters,
            [self::PAGE_URL_PARAMETER => $page]
        );

        return $this->_request->url().'?'.Arr::query($parameters);
    }

    private function validPage()
    {
        $page = $this->_request->input(self::PAGE_URL_PARAMETER);

        return ($page && ($page > 0) && ($page <= $this->totalPages)) ? $page : 1;
    }
}

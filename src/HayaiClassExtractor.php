<?php

namespace D2PRO\Hayai;

use Illuminate\Filesystem\Filesystem;

class HayaiClassExtractor
{
    // private Filesystem $files;

    // public function __construct(Filesystem $files)
    // {
    //     $this->files = $files;
    // }

    public static function extract(string $path): array
    {
        $buffer = '';
        $class = null;
        $namespace = null;

        $fp = fopen($path, 'r');
        $i = 0;
        while (! $class) {
            if (feof($fp)) {
                break;
            }

            $buffer .= fread($fp, 512);
            $tokens = token_get_all($buffer);

            if (strpos($buffer, '{') === false) {
                continue;
            }

            for (;$i < count($tokens);$i++) {
                if ($tokens[$i][0] === T_NAMESPACE) {
                    for ($j = $i + 1;$j < count($tokens); $j++) {
                        if ($tokens[$j][0] === T_STRING) {
                            $namespace .= '\\'.$tokens[$j][1];
                        } elseif ($tokens[$j] === '{' || $tokens[$j] === ';') {
                            break;
                        }
                    }
                }

                if ($tokens[$i][0] === T_CLASS) {
                    for ($j = $i + 1;$j < count($tokens);$j++) {
                        if (($tokens[$j] === '{') && (! $class)) {
                            $class = $tokens[$i + 2][1];
                        }
                    }
                }
            }
        }

        return [$namespace, $class];
    }
}

<?php

namespace D2PRO\Hayai\Facades;

use Illuminate\Support\Facades\Facade;

class Hayai extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'hayai';
    }
}

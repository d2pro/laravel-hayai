<?php

namespace D2PRO\Hayai\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObject;

class DTO extends DataTransferObject
{
    /**
     * DTO::fromSet
     *
     * Transforma una array de arrays (filas/columnas) en un array de objetos DTOs
     *
     * Este método rara vez habrá que sobreescribirlo
    */
    public static function fromSet(array $set): array
    {
        $array = [];
        // To call the 'fromArray' method of the child if it is overrided
        $childClass = get_called_class();

        foreach ($set as $item) {
            $array[] = $childClass::fromArray($item);
        }

        return $array;
    }

    /**
     * DTO::fromArray
     *
     * Por defecto se mapean las mismas claves que vienen en el array $data,
     * así que éstas claves deben existir como propiedades en el DTO
     * (con el mismo nombre que la $key).
     *
     * Si son diferentes los nombres de las propiedades de las keys que trae $data
     * o si es necesario alguna transformación de datos, debemos sobreescribir
     * este método en la clase hija.
     */
    public static function fromArray(array $data): self
    {
        dd("este", $data);
        $dtoData = [];

        foreach ($data as $key => $value) {
            $dtoData[$key] = $value;
        }

        return new self($dtoData);
    }
}

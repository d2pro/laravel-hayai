<?php

namespace D2PRO\Hayai\Fields;

class Boolean extends Field
{
    public static function make($label, $field = null): self
    {
        $self = parent::_make(Boolean::class, 'checkbox', $label, $field);

        return $self;
    }

    /**
     * Use 'checkbox' as component to represent the field value
     */
    public function checkbox()
    {
        $this->component = 'checkbox';

        return $this;
    }

    /**
     * Use 'switch' as component to represent the field value
     */
    public function switch()
    {
        $this->component = 'switch';

        return $this;
    }
}

<?php

namespace D2PRO\Hayai\Fields;

class Textarea extends Field
{
    public static function make($label, $field = null)
    {
        $self = parent::_make(Textarea::class, 'textarea', $label, $field);

        return $self;
    }
}

<?php

namespace D2PRO\Hayai\Fields;

class Password extends Field
{
    public static function make($label, $field = null)
    {
        $self = parent::_make(Password::class, 'password', $label, $field);

        $self->hideFromIndex = true;

        return $self;
    }
}

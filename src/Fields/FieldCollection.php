<?php

namespace D2PRO\Hayai\Fields;

use Illuminate\Support\Collection;

class FieldCollection extends Collection
{
    /**
     * Finds a given field by its field name
     */
    public function findFieldByName(string $fieldName, $default = null): ?Field
    {
        return $this->first(function ($field) use ($fieldName) {
            return isset($field->attribute) &&
                   $field->attribute == $fieldName;
        }, $default);
    }
}

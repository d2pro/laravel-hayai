<?php

namespace D2PRO\Hayai\Fields;

class Text extends Field
{
    public static function make($label, $field = null)
    {
        $self = parent::_make(Text::class, 'text', $label, $field);

        return $self;
    }
}

<?php

namespace D2PRO\Hayai\Fields;

class ID extends Field
{
    public static function make($label = 'ID', $field = 'id')
    {
        $self = parent::_make(ID::class, 'text', $label, $field);

        $self->notEditable()
            ->textAlign('right')
            ->headerClass('w-16')
            ->columnClass('w-16');

        return $self;
    }
}

<?php

namespace D2PRO\Hayai\Fields;

use Illuminate\Support\Str;

class Field
{
    /**
     * The current field value
     */
    public $value;

    /**
     * The Vue component that handles this field
     */
    protected string $component;
    /**
     * The label
     */
    protected string $_label;
    /**
     * The name of the table column
     * If blank, it will be infered from the label (with snake case)
     */
    protected string $_tableField;
    /**
     * Is this field sortable? (for the index page)
     */
    protected bool $_sortable = false;
    /**
     * Callback for field sort resolution
     */
    public $sortByCallback;
    /**
     * Is this field searchable? (for the index page)
     */
    protected bool $_searchable = false;
    /**
     * Callback for field search query
     */
    public $searchByCallback;
    /**
     * Is this field editable? (for the create and edit)
     */
    protected bool $_editable = true;

    /**
     * Hides the field in the index page
     */
    public bool $hideFromIndex = false;

    /**
     * Shows the field in the index page using this callback
     */
    public $showInIndexCallback;

    /**
     * Hides the field in the index page using this callback
     */
    public $hideFromIndexCallback;

    /**
     * The callback to format the field value to display it in index page
     */
    protected $formatForIndexCallback;

    /**
     * Callback to evaluate if the user can see a field (index/creation/edition)
     */
    public $allowViewCallback;

    // Format
    protected string $_textAlign = 'left';
    protected ?string $_headerClass = null; // For list index
    protected ?string $_columnClass = null; // For list index

    // Validation Rules
    protected array $_rules = [];
    protected array $_creationRules = [];
    protected array $_updateRules = [];

    protected static function _make($class, $component, $label, $field = null)
    {
        $self = new $class;

        $self->component = $component;
        $self->_label = $label;
        $self->_tableField = $field ?: Str::snake($label);

        return $self;
    }

    public function tableField()
    {
        return $this->_tableField;
    }

    public function sortable()
    {
        $this->_sortable = true;

        return $this;
    }

    public function sortBy($callback)
    {
        $this->sortByCallback = $callback;

        return $this;
    }

    public function isSortable()
    {
        return $this->_sortable;
    }

    public function searchable()
    {
        $this->_searchable = true;

        return $this;
    }

    public function searchBy($callback)
    {
        $this->searchByCallback = $callback;

        return $this;
    }

    public function isSearchable()
    {
        return $this->_searchable;
    }

    public function notEditable()
    {
        $this->_editable = false;

        return $this;
    }

    public function isEditable()
    {
        return $this->_editable;
    }

    public function showInIndex($callback = null)
    {
        if (is_callable($callback)) {
            $this->showInIndexCallback = $callback;
        } else {
            $this->hideFromIndex = false;
        }

        return $this;
    }

    public function hideFromIndex($callback = null)
    {
        if (is_callable($callback)) {
            $this->hideFromIndexCallback = $callback;
        } else {
            $this->hideFromIndex = true;
        }

        return $this;
    }

    public function withFormatForIndex($callback)
    {
        $this->formatForIndexCallback = $callback;

        return $this;
    }

    public function allowView($callback)
    {
        $this->allowViewCallback = $callback;

        return $this;
    }

    public function textAlign($align)
    {
        $this->_textAlign = $align;

        return $this;
    }

    public function headerClass($class)
    {
        $this->_headerClass = $class;

        return $this;
    }

    public function columnClass($class)
    {
        $this->_columnClass = $class;

        return $this;
    }

    public function rules(...$rules)
    {
        $this->_rules = $rules;

        return $this;
    }

    public function creationRules(...$creationRules)
    {
        $this->_creationRules = $creationRules;

        return $this;
    }

    public function updateRules(...$updateRules)
    {
        $this->_updateRules = $updateRules;

        return $this;
    }

    public function getCreationRules(): array
    {
        return array_merge($this->_rules, $this->_creationRules);
    }

    public function getUpdateRules(): array
    {
        return array_merge($this->_rules, $this->_updateRules);
    }

    /**
     * Loads the field value from model or DTO
     */
    public function load($resource, $tableField = null)
    {
        $tableField ??= $this->tableField();

        $this->value = $this->loadFieldValue($resource, $tableField);
    }

    protected function loadFieldValue($resource, $tableField)
    {
        return data_get($resource, str_replace('->', '.', $tableField));
    }

    public function formatForIndex($resource, $tableField = null)
    {
        $tableField ??= $this->tableField();

        if (! $this->formatForIndexCallback) {
            $this->load($resource, $tableField);
        } elseif (is_callable($this->formatForIndexCallback)) {
            tap($this->loadFieldValue($resource, $tableField), function () use ($resource) {
                $this->value = call_user_func($this->formatForIndexCallback, $resource);
            });
        }
    }

    public function toFront(): array
    {
        $array = [
            'component' => $this->component,
            'label' => $this->_label,
            'field' => $this->_tableField,
            'sortable' => $this->_sortable,
            'searchable' => $this->_searchable,
            'editable' => $this->_editable,
            'align' => $this->_textAlign,
        ];

        if ($this->_headerClass) {
            $array['headerClass'] = $this->_headerClass;
        }
        if ($this->_columnClass) {
            $array['columnClass'] = $this->_columnClass;
        }

        return $array;
    }
}

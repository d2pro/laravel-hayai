<?php

namespace D2PRO\Hayai\Console;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeViewModelCommand extends GeneratorCommand
{
    protected $name = 'hayai:viewmodel';

    protected $description = 'Create a new View Model class file';

    protected $type = 'View Model';

    protected function qualifyClass($name)
    {
        // Si no termina en ...ViewModel
        if (! Str::endsWith($name, 'ViewModel')) {
            $name .= 'ViewModel';
        }

        return parent::qualifyClass($name);
    }

    protected function getPath($name)
    {
        if ($this->option('app')) {
            $path = Str::replaceLast('App', '', $this->laravel['path']);
            return $path.str_replace('\\', '/', $name).'.php';
        }

        return parent::getPath($name);
    }

    protected function rootNamespace()
    {
        if ($app = $this->option('app')) {
            return 'App\\'.$app.'\\';
        }

        return $this->laravel->getNamespace();
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\ViewModels';
    }

    protected function getStub()
    {
        return __DIR__.'/stubs/view-model.stub';
    }

    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the View Model.'],
        ];
    }

    protected function getOptions()
    {
        return [
            ['app', null, InputOption::VALUE_OPTIONAL, 'If you\'re using a app/domain schema, fill the app name.', ''],
        ];
    }
}

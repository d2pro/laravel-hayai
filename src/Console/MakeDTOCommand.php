<?php

namespace D2PRO\Hayai\Console;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeDTOCommand extends GeneratorCommand
{
    protected $name = 'hayai:dto';

    protected $description = 'Create a new Data Transfer Object class file';

    protected $type = 'DTO';

    protected function qualifyClass($name)
    {
        // Si no termina en ...DTO
        if (! Str::endsWith($name, 'DTO')) {
            $name .= 'DTO';
        }

        return parent::qualifyClass($name);
    }

    protected function getPath($name)
    {
        if ($this->option('domain')) {
            $path = Str::replaceLast('App', '', $this->laravel['path']);
            return $path.str_replace('\\', '/', $name).'.php';
        }

        return parent::getPath($name);
    }

    protected function rootNamespace()
    {
        if ($domain = $this->option('domain')) {
            return 'Domain\\'.$domain.'\\';
        }

        return $this->laravel->getNamespace();
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\DataTransferObjects';
    }

    protected function getStub()
    {
        return __DIR__.'/stubs/data-transfer-object.stub';
    }

    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the Data Trasfer Object.'],
        ];
    }

    protected function getOptions()
    {
        return [
            ['domain', null, InputOption::VALUE_OPTIONAL, 'If you\'re using a app/domain schema, fill the domain name.', ''],
        ];
    }
}

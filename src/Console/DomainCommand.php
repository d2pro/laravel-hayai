<?php

namespace D2PRO\Hayai\Console;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;

class DomainCommand extends Command
{
    protected $name = 'hayai:domain';

    protected $description = 'Creates a new domain folder structure';

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function handle()
    {
        $name = $this->getNameInput();
        $root = $this->getDomainRoot($name);

        $this->comment('Creating root folder...');
        $this->makeDirectory($root);
        $this->comment('Creating actions folder...');
        $this->makeDirectory($root.'/Actions/');
        $this->comment('Creating data-transfer-objects folder...');
        $this->makeDirectory($root.'/DataTransferObjects');
        $this->comment('Creating models folder...');
        $this->makeDirectory($root.'/Models');
        $this->comment('Creating query-builders folder...');
        $this->makeDirectory($root.'/QueryBuilders');
        $this->comment('Creating rules folder...');
        $this->makeDirectory($root.'/Rules');

        $this->info('Domain '.$name.' folder structure created successfully.');
    }

    private function getDomainRoot($name)
    {
        $path = Str::replaceLast('App', '', $this->laravel['path']);

        return $path.'Domain/'.$name;
    }

    private function makeDirectory($path)
    {
        if (! $this->files->isDirectory($path)) {
            $this->files->makeDirectory($path, 0777, true, true);
        }

        return $path;
    }

    private function getNameInput()
    {
        return trim($this->argument('name'));
    }

    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the new domain.'],
        ];
    }
}

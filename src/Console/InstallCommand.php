<?php

namespace D2PRO\Hayai\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class InstallCommand extends Command
{
    private Filesystem $files;

    protected $signature = 'hayai:install {--app-domain : Indicates if you\'re using an app/domain schema} {--force}';

    protected $description = 'Install all of the Hayai resources';

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function handle()
    {
        $force = $this->option('force');
        // $this->comment('Publishing Hayai Service Provider...');
        // $this->callSilent('vendor:publish', ['--tag' => 'hayai-provider']);

        // $this->comment('Publishing Hayai Assets...');
        // $this->callSilent('vendor:publish', ['--tag' => 'hayai-assets']);

        $this->comment('Publishing Hayai Configuration...');
        if ($this->files->exists(config_path('hayai.php'))) {
            if ($force) {
                $this->callSilent('vendor:publish', ['--tag' => 'hayai-config', '--force' => null]);
            } else {
                $this->error('The \'hayai.php\' config file already exists!');
                $this->line('Use --force option to recreate/overwrite it');
            }
        } else {
            $this->callSilent('vendor:publish', ['--tag' => 'hayai-config']);
        }
        $this->saveAppDomainOption();

        $this->info('Hayai scaffolding installed successfully.');
    }

    /**
     * Modifica el config (hayai.php) para poner la opción 'searchByAppDomain'
     * a 'true' en el caso de estemos usando una app con estructura 'app/domain'
     * de carpetas
     */
    private function saveAppDomainOption()
    {
        if (! $this->option('app-domain')) {
            return;
        }

        $path = config_path('hayai.php');
        $configFile = $this->files->get($path);
        $configFile = str_replace(
            "'searchByAppDomain' => false,",
            "'searchByAppDomain' => true,",
            $configFile
        );
        $this->files->put($path, $configFile);
    }
}

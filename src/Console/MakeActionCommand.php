<?php

namespace D2PRO\Hayai\Console;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeActionCommand extends GeneratorCommand
{
    protected $name = 'hayai:action';

    protected $description = 'Create a new Action class file';

    protected $type = 'Action class';

    protected function qualifyClass($name)
    {
        // Si no termina en ...Action
        if (! Str::endsWith($name, 'Action')) {
            $name .= 'Action';
        }

        return parent::qualifyClass($name);
    }

    protected function getPath($name)
    {
        if ($this->option('domain')) {
            $path = Str::replaceLast('App', '', $this->laravel['path']);
            return $path.str_replace('\\', '/', $name).'.php';
        }

        return parent::getPath($name);
    }

    protected function rootNamespace()
    {
        if ($domain = $this->option('domain')) {
            return 'Domain\\'.$domain.'\\';
        }

        return $this->laravel->getNamespace();
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Actions';
    }

    protected function getStub()
    {
        return __DIR__.'/stubs/action.stub';
    }

    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the action.'],
        ];
    }

    protected function getOptions()
    {
        return [
            ['domain', null, InputOption::VALUE_OPTIONAL, 'If you\'re using a app/domain schema, fill the domain name.', ''],
        ];
    }
}

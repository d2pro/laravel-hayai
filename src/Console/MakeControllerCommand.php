<?php

namespace D2PRO\Hayai\Console;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeControllerCommand extends GeneratorCommand
{
    protected $name = 'hayai:controller';

    protected $description = 'Create a new Controller (invokable) class file';

    protected $type = 'Controller';

    protected function qualifyClass($name)
    {
        // Si no termina en ...Controller
        if (! Str::endsWith($name, 'Controller')) {
            $name .= 'Controller';
        }

        return parent::qualifyClass($name);
    }

    protected function getPath($name)
    {
        if ($this->option('app')) {
            $path = Str::replaceLast('App', '', $this->laravel['path']);
            return $path.str_replace('\\', '/', $name).'.php';
        }

        return parent::getPath($name);
    }

    protected function rootNamespace()
    {
        if ($app = $this->option('app')) {
            return 'App\\'.$app.'\\';
        }

        return $this->laravel->getNamespace();
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Controllers';
    }

    protected function getStub()
    {
        return __DIR__.'/stubs/controller.stub';
    }

    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the controller.'],
        ];
    }

    protected function getOptions()
    {
        return [
            ['app', null, InputOption::VALUE_OPTIONAL, 'If you\'re using a app/domain schema, fill the app name.', ''],
        ];
    }
}

<?php

namespace D2PRO\Hayai\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;

class AppCommand extends Command
{
    protected $name = 'hayai:app';

    protected $description = 'Creates a new app folder structure';

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function handle()
    {
        $name = $this->getNameInput();
        $root = $this->getAppRoot($name);

        $this->comment('Creating root folder...');
        $this->makeDirectory($root);
        $this->comment('Creating controllers folder...');
        $this->makeDirectory($root.'/Controllers/');
        $this->comment('Creating middlewares folder...');
        $this->makeDirectory($root.'/Middlewares/');
        $this->comment('Creating requests folder...');
        $this->makeDirectory($root.'/Requests');
        $this->comment('Creating resources folder...');
        $this->makeDirectory($root.'/Resources');
        $this->comment('Creating routes folder...');
        $this->makeDirectory($root.'/Routes');
        $this->comment('Creating view-models folder...');
        $this->makeDirectory($root.'/ViewModels');

        $this->info('Domain '.$name.' folder structure created successfully.');
    }

    private function getAppRoot($name)
    {
        return $this->laravel['path'].'/'.$name;
    }

    private function makeDirectory($path)
    {
        if (! $this->files->isDirectory($path)) {
            $this->files->makeDirectory($path, 0777, true, true);
        }

        return $path;
    }

    private function getNameInput()
    {
        return trim($this->argument('name'));
    }

    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the new app.'],
        ];
    }
}

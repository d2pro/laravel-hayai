<?php

namespace D2PRO\Hayai\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HayaiRequest extends FormRequest
{
    public ?string $sortField = null;
    public ?string $sortDirection = null;
    public ?string $searchQuery = null;

    public function authorize()
    {
        // Tomamos los valores para odenar y buscar
        if ($sort = $this->input('s')) {
            if ($parts = explode('|', $sort)) {
                $this->sortField = $parts[0];
                $this->sortDirection = count($parts) > 1 ? $parts[1] : null;
            }
        }
        $this->searchQuery = $this->input('q');
        // dd('request', $this->sortField, $this->sortDirection, $this->searchQuery);
        return true;
    }

    public function rules()
    {
        return [];
    }

    /**
     * Get the class name of the resource being requested
     *
     * @return mixed
     */
    // public function resource()
    // {
    //     return tap(Nova::resourceForKey($this->route('resource')), function ($resource) {
    //         abort_if(is_null($resource), 404);
    //         abort_if(! $resource::authorizedToViewAny($this), 403);
    //     });
    // }
}

<?php

namespace D2PRO\Hayai\Contracts;

/**
 * Interface Action
 * @method execute
 */
interface Action
{
}
